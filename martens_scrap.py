import bs4
import os
import sqlite3
import streamlit as st
import urllib.request as req
from email.mime.text import MIMEText
from Google import Create_Service
from email.mime.multipart import MIMEMultipart
import base64

gmail_user = os.environ.get('GMAIL_USER')
gmail_password = os.environ.get('GMAIL_PASSWORD')
CLIENT_SECRET_FILE = 'client_id.json'
API_NAME = 'gmail'
API_VERSION = 'v1'
SCOPES = ['https://mail.google.com/']

url = "https://www.drmartens.com/us/en/p/11822006"
request = req.Request(url, headers={
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36"
})

def web_scrap():
    with req.urlopen(url) as response:
        data = response.read().decode('utf-8')

    root = bs4.BeautifulSoup(data, "html.parser")
    titles = root.find("span", class_="bfx-price")
    return float(titles.string.split('$')[1])

def add_table_email(email):
    conn = sqlite3.connect('state.db')
    cursor = conn.cursor()
    
    cursor.execute("INSERT INTO email VALUES ('{}')".format(email))
    
    conn.commit()
    conn.close()

def delete_table_email(email):
    conn = sqlite3.connect('state.db')
    cursor = conn.cursor()
    
    cursor.execute("DELETE FROM email WHERE Email='{}'".format(email))

    conn.commit()
    conn.close()

def get_table_email():
    conn = sqlite3.connect('state.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM email')

    results = cursor.fetchall()
    conn.commit()
    conn.close()
    return results


def set_send_status(data):
    conn = sqlite3.connect('state.db')
    cursor = conn.cursor()

    if get_send_status() == True:
        cursor.execute("UPDATE status SET SendBool=0")
    else:
        cursor.execute("UPDATE status SET SendBool=1")

    conn.commit()
    conn.close()


def get_send_status():
    conn = sqlite3.connect('state.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM status')

    results = cursor.fetchall()
    conn.commit()
    conn.close()

    return False if list(results[0])[0] == 0 else True
    
def gmail_api_send_email(receivers, data):
    service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)

    emailMsg = """哈摟~~今天的 Dr. Martens 的經經經典款價錢是 ${} 喔~~~""".format(data)
    mimeMessage = MIMEMultipart()
    mimeMessage['to'] = ", ".join(receivers)
    mimeMessage['subject'] = 'Dr.Martens daily price'
    mimeMessage.attach(MIMEText(emailMsg, 'plain'))
    raw_string = base64.urlsafe_b64encode(mimeMessage.as_bytes()).decode()
    
    message = service.users().messages().send(userId='me', body={'raw': raw_string}).execute()
    print(message)

def main():
    st.title("Send Dr.Martens price Automatically")
    expand_bar = st.beta_expander("About")
    
    expand_bar.markdown("""
    * This app will send **1460 SMOOTH LEATHER LACE UP BOOTS** price to your E-mail everyday.
    * [Prcie form the official website](https://www.drmartens.com/us/en/p/11822006)
    """)
    st.write("")
    
    st.markdown(" ### User settings")
    email = st.text_input("Enter your E-mail")
    
    add_email = st.button("Subscribe")
    delete_email = st.button("UnSubscribe")
    
    if add_email == True:
        add_table_email(email)
    if delete_email == True:
        delete_table_email(email)
    
    emails = get_table_email()
    emails_list = []
    for i in emails[1:]:
        emails_list.append(i[0])
    st.sidebar.selectbox("Enrolled Email", emails_list)
    
    st.markdown("***")
    st.markdown(" ### Enable auto sending E-mail function")
    
    click = st.button("Send E-mail")
    if click == True:
        set_send_status(click)
    status = get_send_status()
    if status == True:
        st.success("Connected")
        price = web_scrap()
        gmail_api_send_email(emails_list, price)
    else:
        st.warning("Disconnected")


if __name__ == '__main__':
    main()
