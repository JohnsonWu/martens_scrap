import sqlite3

def create_table_status():
    conn = sqlite3.connect('state.db')
    cursor = conn.cursor()

    # create table
    cursor.execute(""" CREATE TABLE IF NOT EXISTS status (
                SendBool integer
                    ) """)
    cursor.execute("INSERT INTO status VALUES ({})".format(1))

    conn.commit()
    conn.close()

def create_table_email():
    conn = sqlite3.connect('state.db')
    cursor = conn.cursor()

    # create table
    cursor.execute(""" CREATE TABLE IF NOT EXISTS email (
                Email varchar(255)
                    ) """)
    cursor.execute("INSERT INTO email VALUES ({})".format(1))

    conn.commit()
    conn.close()
    
def main():
    create_table_status()
    create_table_email()
    
if __name__ == '__main__':
    main()